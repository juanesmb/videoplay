/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Estudiante
 */
public class VideoDTO {
    private int id;
    private String url;
    private String nombre;
    private String idioma;
    private String categorias;
    private String descripcion;
    
    public VideoDTO(int id, String url, String nombre, String idioma, String categorias) {
        this.id = id;
        this.url = url;
        this.nombre = nombre;
        this.idioma = idioma;
        this.categorias = categorias;
    }
    
    public VideoDTO() {
        
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    public String getCategorias() {
        return categorias;
    }

    public void setCategorias(String categorias) {
        this.categorias = categorias;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "VideoDTO{" + "id=" + id + ", url=" + url + ", nombre=" + nombre + ", idioma=" + idioma + ", categorias=" + categorias + ", descripcion=" + descripcion + '}';
    }
    
}
