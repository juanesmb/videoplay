package modelo;
import baseDatos.MysqlDbAdapter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.List;

public class VideoDAO implements VideoManager{

    List<VideoDTO> videos;
    
    @Override
    public void agregarVideo(VideoDTO video) {
        MysqlDbAdapter conexion = new MysqlDbAdapter();
        Connection Connection = conexion.getConnection();
        
        try{
        PreparedStatement statement = Connection.prepareStatement("INSERT INTO video(id,url,nombre,idioma,categorias,descripcion) VALUES (NULL,?,?,?,?,?)");
        statement.setString(1, video.getUrl());
        statement.setString(2,video.getNombre());
        statement.setString(3,video.getIdioma());
        statement.setString(4,video.getCategorias());
        statement.setString(5,video.getDescripcion());
        statement.executeUpdate();
        }catch(Exception e){
         System.err.print(e.getMessage());
        }
    }
    

    @Override
    public List<VideoDTO> mostrarVideo() {
        return videos;
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void borrarVideo(int id) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void actualizarVideo(int id) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
